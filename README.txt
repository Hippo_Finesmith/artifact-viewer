Compiling Artifact Viewer (Note these instructions are for a 32 bit windows xp machine as it is the only thing I have:

Necessary software

    Now builds with Visual Studio 2008
    
    http://www.microsoft.com/downloads/details.aspx?familyid=7B0B0339-613A-46E6-AB4D-080D4D4A8C4E&displaylang=en    SP1
    http://www.microsoft.com/downloads/details.aspx?FamilyID=90e2942d-3ad1-4873-a2ee-4acc0aace5b6&displaylang=en    (SP1 for Vista)
    
    Visual Studio 2005 redistributable (only for Express Editions or if you're not using Visual Studio 2005 Professional). 
    http://www.microsoft.com/downloads/details.aspx?FamilyID=200b2fd9-ae1a-4a14-984d-389c36f85647&displaylang=en
    
    CMake 2.6.2 or greater. Add CMake to your system PATH.
    http://www.cmake.org/cmake/resources/software.html
    
    ActivePython 2.5 or 2.6.
    http://www.activestate.com/activepython/downloads
    
    Cygwin. Make sure patchutils, flex, and bison are included under "devel". DON'T add cygwin to your PATH.
    http://www.cygwin.com/
    
    Microsoft DirectX SDK November 2008. Install x86 Headers and Libs.
    http://www.microsoft.com/downloads/details.aspx?FamilyId=5493F76A-6D37-478D-BA17-28B1CCA4865A&displaylang=en
    
    Windows SDK 6.1. The only required portion is Developer Tools > Windows x86 Headers and Libraries. On Windows 7 64 bit, you'll also need the 32 bit development tools (otherwise rc.exe isn't found). 
    http://www.microsoft.com/downloads/details.aspx?FamilyId=E6E1C3DF-A74F-4207-8586-711EBE331CDC&displaylang=en

     For the Installer to be created you will need to also install NSIS Unicode.
     http://www.scratchpaper.com/home/downloads
     
     
    Visual C++ 2005/2008 Express Edition Configuration

    Configure VC++ Express by going to Tools > Options > Projects and Solutions > VC++ Directories and adding the following paths to the top of the matching list (upper right dropdown). The ones listed here are default installations; use your local versions:
        Executable files:
            C:\Program Files\CMake 2.6\bin
            C:\Python26
            C:\cygwin\bin (add this to the bottom of the list!) 
        Include files:
            C:\Program Files\Microsoft DirectX SDK (November 2008)\Include
            C:\Program Files\Microsoft SDKs\Windows\v6.1\Include 
        Library files:
            C:\Program Files\Microsoft DirectX SDK (November 2008)\Lib\x86
            C:\Program Files\Microsoft SDKs\Windows\v6.1\Lib 


     Generating Build Files 
     
     Firstly using GIT you need to grab the source code.
     I wont go into how to as I asume if you are attempting to compile a viewer you are at least able to use Git.
     I will say however that I always use something like c:\viewer\ to make my life simple.
     
     Once you have downloaded the source code, navigate to c:\viewer\artifact viewer\indra and run dev.bat and it      will check out the environment and setup the build structure for you.
     
     Once that has finished you can load the project into visual c++ express by opening visual studio 2005 from      start menu and 
     selecting File,Open,Project or Solution and navigating to c:\viewer\artifact viewer\indra\build-vc80 and      selecting Secondlife-bin.sln.
     
     The default StartUp Project should be "Secondlife-bin" (you can tell because it's bold). If not, make it so by      right clicking "Secondlife-bin" and selecting "Set As StartUp Project". 
     There will be three build options: Release, Debug, and RelWithDebInfo. I recommend building Release.
     
     
     Click Build and select Build Solution
     
     Go grab some lunch, a full build takes a while.
     
     
     Once you have completed a build you will find your newly built files in c:\viewer\artifact      viewer\indra\build-vc80\newview\release. 
    
     
     Now you can double click Artifact-Viewer.exe and have fun.
     To install Artifact Viwer simply click the Setup.exe