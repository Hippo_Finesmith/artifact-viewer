#!/bin/bash

# Written by Mana Janus

set -e

os=`sh hippo/osdetect.sh`


# Linden artwork
url_linden_artwork=http://automated-builds-secondlife-com.s3.amazonaws.com/viewer-rc-frozen/slviewer-artwork-viewer-rc-frozen-1.23.5.136274.zip

# Linden libs
url_linden_libs_linux=http://automated-builds-secondlife-com.s3.amazonaws.com/viewer-rc-frozen/slviewer-linux-libs-viewer-rc-frozen-1.23.5.136274.tar.gz
url_linden_libs_cygwin=http://automated-builds-secondlife-com.s3.amazonaws.com/viewer-rc-frozen/slviewer-win32-libs-viewer-rc-frozen-1.23.5.136274.zip

# FMOD
url_fmod_linux=http://www.fmod.org/index.php/release/version/fmodapi375linux.tar.gz
url_fmod_cygwin=http://www.fmod.org/index.php/release/version/fmodapi375win.zip

# Hippo
url_hippo_artwork=http://forge.opensimulator.org/gf/download/frsrelease/214/759/hippo_artwork_v0.5.zip

# Status file
status_file=installed.lst


# the install function uses the same download cache as the linden
# install.xml mechanism:
#
#   /var/tmp/$USER/install.cache/

function install
{
	url=$1
	file=`basename $url`  # yes, this works on URLs too
	if grep ^$file\$ $status_file >/dev/null; then
		# skip files already installed
		true 
	else
		echo "Installing $file"
		cache="/var/tmp/$USER/install.cache/$file"
		if [ ! -r $cache ]; then
			wget -O $cache $url || rm $cache
		fi
		rm -f linden
		ln -s . linden
		case $file in
			*.zip)     unzip -o $cache >/dev/null;;
			*.tar.gz)  tar xzf $cache ;;
			*.tar.bz2) tar xjf $cache ;;
			*) echo "cannot unpack '$file'"; exit 1 ;;
		esac
		rm -f linden
		echo $file >>$status_file
	fi
}

function osVar
{
	varBase=$1
	var=\$$varBase$os
	osValue=`eval echo $var`
	if [ -z "$osValue" ]; then
		echo "Missing variable '$varBase$os'" >&2
		exit 1
	fi
}


# create status file, if not exists
[ -r $status_file ] || touch $status_file

# create cache, if not exists
[ -d /var/tmp/$USER/install.cache/ ] || mkdir -p /var/tmp/$USER/install.cache/

# install Linden libraries
osVar url_linden_libs_
install $osValue

# install linden artwork
install $url_linden_artwork

# cleanup SecondLife files replaced by Hippo
rm -f indra/newview/res/icon1.ico
rm -f indra/newview/res/ll_icon.*
rm -f indra/newview/skins/default/html/en-us/loading/sl_logo_rotate_black.gif

# install FMOD
osVar url_fmod_
install $osValue
if [ -d fmodapi375linux ]; then
	chmod -R +w fmodapi375linux/
	mkdir -p libraries/include/
	mkdir -p libraries/i686-linux/lib_debug/
    mkdir -p libraries/i686-linux/lib_release/
    mkdir -p libraries/i686-linux/lib_release_client/
	cp fmodapi375linux/api/inc/* libraries/include/
	cp fmodapi375linux/api/libfmod-3.75.so libraries/i686-linux/lib_debug/
	cp fmodapi375linux/api/libfmod-3.75.so libraries/i686-linux/lib_release/
	cp fmodapi375linux/api/libfmod-3.75.so libraries/i686-linux/lib_release_client/
fi
if [ -d fmodapi375win ]; then
	chmod -R +w fmodapi375win/
	mkdir -p libraries/include/
	mkdir -p libraries/i686-win32/lib/debug/
	mkdir -p libraries/i686-win32/lib/release/
	cp fmodapi375win/api/inc/* libraries/include/
	cp fmodapi375win/api/fmod.dll indra/newview/
	cp fmodapi375win/api/lib/fmodvc.lib libraries/i686-win32/lib/debug/
	cp fmodapi375win/api/lib/fmodvc.lib libraries/i686-win32/lib/release/
fi
rm -rf fmodapi375*/

# install Hippo OpenSim Viewer artwork
install $url_hippo_artwork

