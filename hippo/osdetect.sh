#!/bin/sh

# Written by Mana Janus

set -e

if (uname | fgrep -i linux >/dev/null); then
    echo "linux"
    exit
fi

if (uname | fgrep -i cygwin >/dev/null); then
    echo "cygwin"
    exit
fi

echo Unknown system `uname` >&2
exit 1

