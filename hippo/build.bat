@echo off

call "%VS80COMNTOOLS%\vsvars32.bat"

echo ******************************
echo Configuring...
echo ******************************

cd indra
if not exist build-VC80 python ./develop.py -G VC80 configure

echo ******************************
echo Building...
echo ******************************

cd build-vc80
vcbuild /M2 /useenv SecondLife.sln "Release|Win32"

echo ******************************
echo Packaging...
echo ******************************

cd newview
python ..\..\newview\viewer_manifest.py --configuration=Release --source=..\..\newview --artwork=..\..\newview --build=. --dest=packaged

