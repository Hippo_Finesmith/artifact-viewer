#!/bin/sh

# Written by Mana Janus

set -e

os=`sh hippo/osdetect.sh`
echo "Building on $os"

# install additional files
bash hippo/install.sh

if [ "$os" = "linux" ]; then
    cd indra
    [ -d viewer-linux-i686-release ] || ./develop.py -t Release configure
    find -name "*.res" | xargs --no-run-if-empty rm
    ./develop.py -t Release build
    cd ..
fi

if [ "$os" = "cygwin" ]; then
    cmd /C hippo\\build.bat
    #scripts/installer.sh
fi

