/** 
 * @file hbprefscool.cpp
 * @author Henri Beauchamp
 * @brief Cool SL Viewer preferences panel
 *
 * $LicenseInfo:firstyear=2008&license=viewergpl$
 * 
 * Copyright (c) 2008, Henri Beauchamp.
 * 
 * Second Life Viewer Source Code
 * The source code in this file ("Source Code") is provided by Linden Lab
 * to you under the terms of the GNU General Public License, version 2.0
 * ("GPL"), unless you have obtained a separate licensing agreement
 * ("Other License"), formally executed by you and Linden Lab.  Terms of
 * the GPL can be found in doc/GPL-license.txt in this distribution, or
 * online at http://secondlifegrid.net/programs/open_source/licensing/gplv2
 * 
 * There are special exceptions to the terms and conditions of the GPL as
 * it is applied to this Source Code. View the full text of the exception
 * in the file doc/FLOSS-exception.txt in this software distribution, or
 * online at http://secondlifegrid.net/programs/open_source/licensing/flossexception
 * 
 * By copying, modifying or distributing this software, you acknowledge
 * that you have read and understood your obligations described above,
 * and agree to abide by those obligations.
 * 
 * ALL LINDEN LAB SOURCE CODE IS PROVIDED "AS IS." LINDEN LAB MAKES NO
 * WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY,
 * COMPLETENESS OR PERFORMANCE.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "hbprefscool.h"

#include "llstartup.h"
#include "llviewercontrol.h"
#include "lluictrlfactory.h"
#include "llcombobox.h"

class HippoPrefsImpl : public LLPanel
{
public:
	HippoPrefsImpl();
	/*virtual*/ ~HippoPrefsImpl() { };

	virtual void refresh();

	void apply();
	void cancel();

private:
	static void onCommitCheckBox(LLUICtrl* ctrl, void* user_data);
	void refreshValues();
	BOOL mShowGrids;
	BOOL mSaveScriptsAsMono;
	BOOL mDoubleClickTeleport;
	BOOL mHideNotificationsInChat;
	BOOL mDisableMessagesSpacing;
	BOOL mHideMasterRemote;
	BOOL mShowGroupsButton;
	BOOL mUseOldChatHistory;
	BOOL mUseOldStatusBarIcons;
	BOOL mUseOldTrackingDots;
	BOOL mAllowMUpose;
	BOOL mAutoCloseOOC;
	BOOL mPlayTypingSound;
	BOOL mPrivateLookAt;
	BOOL mFetchInventoryOnLogin;
	BOOL mRestrainedLove;
	BOOL mSecondsInChatAndIMs;
	BOOL mPreviewAnimInWorld;
	BOOL mSpeedRez;
	BOOL mRevokePermsOnStandUp;
	BOOL mRezWithLandGroup;
	U32 mSpeedRezInterval;
	U32 mDecimalsForTools;
	U32 mLinksForChattingObjects;
	U32 mTimeFormat;
	U32 mDateFormat;
};


HippoPrefsImpl::HippoPrefsImpl()
 : LLPanel("Hippo Prefs Panel")
{
	LLUICtrlFactory::getInstance()->buildPanel(this, "panel_preferences_cool.xml");
	childSetCommitCallback("restrained_life_check", onCommitCheckBox, this);
	childSetCommitCallback("speed_rez_check", onCommitCheckBox, this);
	refresh();
}

//static
void HippoPrefsImpl::onCommitCheckBox(LLUICtrl* ctrl, void* user_data)
{
	HippoPrefsImpl* self = (HippoPrefsImpl*)user_data;

	if (self->childGetValue("restrained_life_check").asBoolean())
	{
		gSavedSettings.setBOOL("FetchInventoryOnLogin",	TRUE);
		self->childSetValue("fetch_inventory_on_login_check", TRUE);
		self->childDisable("fetch_inventory_on_login_check");
	}
	else
	{
		self->childEnable("fetch_inventory_on_login_check");
	}

	if (self->childGetValue("speed_rez_check").asBoolean())
	{
		self->childEnable("speed_rez_interval");
		self->childEnable("speed_rez_seconds");
	}
	else
	{
		self->childDisable("speed_rez_interval");
		self->childDisable("speed_rez_seconds");
	}
}

void HippoPrefsImpl::refreshValues()
{
	mSaveScriptsAsMono			= gSavedSettings.getBOOL("SaveScriptsAsMono");
	mDoubleClickTeleport		= gSavedSettings.getBOOL("DoubleClickTeleport");
	mHideNotificationsInChat	= gSavedSettings.getBOOL("HideNotificationsInChat");
	mDisableMessagesSpacing		= gSavedSettings.getBOOL("DisableMessagesSpacing");
	mHideMasterRemote			= gSavedSettings.getBOOL("HideMasterRemote");
	mShowGroupsButton			= gSavedSettings.getBOOL("ShowGroupsButton");
	mUseOldChatHistory			= gSavedSettings.getBOOL("UseOldChatHistory");
	mUseOldStatusBarIcons		= gSavedSettings.getBOOL("UseOldStatusBarIcons");
	mUseOldTrackingDots			= gSavedSettings.getBOOL("UseOldTrackingDots");
	mAllowMUpose				= gSavedSettings.getBOOL("AllowMUpose");
	mAutoCloseOOC				= gSavedSettings.getBOOL("AutoCloseOOC");
	mPlayTypingSound			= gSavedSettings.getBOOL("PlayTypingSound");
	mPrivateLookAt				= gSavedSettings.getBOOL("PrivateLookAt");
	mSecondsInChatAndIMs		= gSavedSettings.getBOOL("SecondsInChatAndIMs");
	mRestrainedLove				= gSavedSettings.getBOOL("RestrainedLove");
	if (mRestrainedLove)
	{
		mFetchInventoryOnLogin	= TRUE;
		gSavedSettings.setBOOL("FetchInventoryOnLogin",	TRUE);
	}
	else
	{
		mFetchInventoryOnLogin	= gSavedSettings.getBOOL("FetchInventoryOnLogin");
	}
	mPreviewAnimInWorld			= gSavedSettings.getBOOL("PreviewAnimInWorld");
	mSpeedRez					= gSavedSettings.getBOOL("SpeedRez");
	mSpeedRezInterval			= gSavedSettings.getU32("SpeedRezInterval");
	mDecimalsForTools			= gSavedSettings.getU32("DecimalsForTools");
	mLinksForChattingObjects	= gSavedSettings.getU32("LinksForChattingObjects");
	mRevokePermsOnStandUp		= gSavedSettings.getBOOL("RevokePermsOnStandUp");
	mRezWithLandGroup			= gSavedSettings.getBOOL("RezWithLandGroup");
}

void HippoPrefsImpl::refresh()
{
	refreshValues();

	if (LLStartUp::getStartupState() != STATE_STARTED)
	{
		childDisable("restrained_life_check");
	}

	if (mRestrainedLove)
	{
		childSetValue("fetch_inventory_on_login_check", TRUE);
		childDisable("fetch_inventory_on_login_check");
	}
	else
	{
		childEnable("fetch_inventory_on_login_check");
	}

	if (mSpeedRez)
	{
		childEnable("speed_rez_interval");
		childEnable("speed_rez_seconds");
	}
	else
	{
		childDisable("speed_rez_interval");
		childDisable("speed_rez_seconds");
	}

	std::string format = gSavedSettings.getString("ShortTimeFormat");
	if (format.find("%p") == -1)
	{
		mTimeFormat = 0;
	}
	else
	{
		mTimeFormat = 1;
	}

	format = gSavedSettings.getString("ShortDateFormat");
	if (format.find("%m/%d/%") != -1)
	{
		mDateFormat = 2;
	}
	else if (format.find("%d/%m/%") != -1)
	{
		mDateFormat = 1;
	}
	else
	{
		mDateFormat = 0;
	}

	// time format combobox
	LLComboBox* combo = getChild<LLComboBox>("time_format_combobox");
	if (combo)
	{
		combo->setCurrentByIndex(mTimeFormat);
	}

	// date format combobox
	combo = getChild<LLComboBox>("date_format_combobox");
	if (combo)
	{
		combo->setCurrentByIndex(mDateFormat);
	}
}

void HippoPrefsImpl::cancel()
{
	gSavedSettings.setBOOL("SaveScriptsAsMono",			mSaveScriptsAsMono);
	gSavedSettings.setBOOL("DoubleClickTeleport",		mDoubleClickTeleport);
	gSavedSettings.setBOOL("HideNotificationsInChat",	mHideNotificationsInChat);
	gSavedSettings.setBOOL("DisableMessagesSpacing",	mDisableMessagesSpacing);
	gSavedSettings.setBOOL("HideMasterRemote",			mHideMasterRemote);
	gSavedSettings.setBOOL("ShowGroupsButton",			mShowGroupsButton);
	gSavedSettings.setBOOL("UseOldChatHistory",			mUseOldChatHistory);
	gSavedSettings.setBOOL("UseOldStatusBarIcons",		mUseOldStatusBarIcons);
	gSavedSettings.setBOOL("UseOldTrackingDots",		mUseOldTrackingDots);
	gSavedSettings.setBOOL("AllowMUpose",				mAllowMUpose);
	gSavedSettings.setBOOL("AutoCloseOOC",				mAutoCloseOOC);
	gSavedSettings.setBOOL("PlayTypingSound",			mPlayTypingSound);
	gSavedSettings.setBOOL("PrivateLookAt",				mPrivateLookAt);
	gSavedSettings.setBOOL("FetchInventoryOnLogin",		mFetchInventoryOnLogin);
	gSavedSettings.setBOOL("RestrainedLove",			mRestrainedLove);
	gSavedSettings.setBOOL("SecondsInChatAndIMs",		mSecondsInChatAndIMs);
	gSavedSettings.setBOOL("PreviewAnimInWorld",		mPreviewAnimInWorld);
	gSavedSettings.setBOOL("SpeedRez",					mSpeedRez);
	gSavedSettings.setU32("SpeedRezInterval",			mSpeedRezInterval);
	gSavedSettings.setU32("DecimalsForTools",			mDecimalsForTools);
	gSavedSettings.setU32("LinksForChattingObjects",	mLinksForChattingObjects);
	gSavedSettings.setBOOL("RevokePermsOnStandUp",		mRevokePermsOnStandUp);
	gSavedSettings.setBOOL("RezWithLandGroup",			mRezWithLandGroup);
}

void HippoPrefsImpl::apply()
{
	std::string short_date, long_date, short_time, long_time, timestamp;	

	LLComboBox* combo = getChild<LLComboBox>("time_format_combobox");
	if (combo) {
		mTimeFormat = combo->getCurrentIndex();
	}

	combo = getChild<LLComboBox>("date_format_combobox");
	if (combo)
	{
		mDateFormat = combo->getCurrentIndex();
	}

	if (mTimeFormat == 0)
	{
		short_time = "%H:%M";
		long_time  = "%H:%M:%S";
		timestamp  = " %H:%M:%S";
	}
	else
	{
		short_time = "%I:%M %p";
		long_time  = "%I:%M:%S %p";
		timestamp  = " %I:%M %p";
	}

	if (mDateFormat == 0)
	{
		short_date = "%Y-%m-%d";
		long_date  = "%A %d %B %Y";
		timestamp  = "%a %d %b %Y" + timestamp;
	}
	else if (mDateFormat == 1)
	{
		short_date = "%d/%m/%Y";
		long_date  = "%A %d %B %Y";
		timestamp  = "%a %d %b %Y" + timestamp;
	}
	else
	{
		short_date = "%m/%d/%Y";
		long_date  = "%A, %B %d %Y";
		timestamp  = "%a %b %d %Y" + timestamp;
	}

	gSavedSettings.setString("ShortDateFormat",	short_date);
	gSavedSettings.setString("LongDateFormat",	long_date);
	gSavedSettings.setString("ShortTimeFormat",	short_time);
	gSavedSettings.setString("LongTimeFormat",	long_time);
	gSavedSettings.setString("TimestampFormat",	timestamp);

	refreshValues();
}

//---------------------------------------------------------------------------

HippoPrefs::HippoPrefs()
:	impl(* new HippoPrefsImpl())
{
}

HippoPrefs::~HippoPrefs()
{
	delete &impl;
}

void HippoPrefs::apply()
{
	impl.apply();
}

void HippoPrefs::cancel()
{
	impl.cancel();
}

LLPanel* HippoPrefs::getPanel()
{
	return &impl;
}
